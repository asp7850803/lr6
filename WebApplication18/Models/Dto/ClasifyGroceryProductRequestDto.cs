﻿using System.Text.Json.Serialization;

namespace WebApplication18.Models.Dto
{
    public class ClasifyGroceryProductRequestDto
    {
        [JsonPropertyName("plu_code")]
        public string PluCode { get; set; }
        public string Title { get; set; }
        public string Upc { get; set; }
    }
}
