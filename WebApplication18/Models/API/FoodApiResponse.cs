﻿using System.Text.Json.Serialization;

namespace WebApplication18.Models.API
{
    public class SummaryRecipeResponse
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("summary")]
        public string Summary { get; set; }
    }

    public class ClassifyGroceryProductResponse
    {
        [JsonPropertyName("matched")]
        public string Matched { get; set; }
        [JsonPropertyName("breadcrumbs")]
        public string[] Breadcrumbs { get; set; }
        [JsonPropertyName("category")]
        public string Category { get; set; }
        [JsonPropertyName("usdaCode")]
        public int UsdaCode { get; set; }
        [JsonPropertyName("image")]
        public string Image { get; set; }
        [JsonPropertyName("cleanTitle")]
        public string CleanTitle { get; set; }
        [JsonPropertyName("ingredientId")]
        public int IngredientId { get; set; }
    }
}
