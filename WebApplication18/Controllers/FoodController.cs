﻿using Microsoft.AspNetCore.Mvc;
using WebApplication18.Models.API;
using WebApplication18.Models.Dto;
using WebApplication18.Services.FoodService;

namespace WebApplication18.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FoodController : ControllerBase
    {
        private readonly IFoodService _foodService;
        private readonly ILogger<FoodController> _logger;

        public FoodController(IFoodService foodService, ILogger<FoodController> logger)
        {
            _foodService = foodService;
            _logger = logger;
        }

        [HttpGet("{id:int}")]
        public async Task<ResponseDto<SummaryRecipeResponse>> GetSummaryRecipe(int id)
        {
            return await this._foodService.Get(id);
        }

        [HttpPost]
        public async Task<ResponseDto<ClassifyGroceryProductResponse>> ClassifyGroceryProduct(ClasifyGroceryProductRequestDto dto)
        {
            return await this._foodService.Post(dto);
        }
    }
}
