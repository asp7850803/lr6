using WebApplication18.Services.FoodService;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
//var apiKey = builder.Configuration.GetValue<string>("foodApiKey");
//Console.WriteLine(apiKey);
builder.Services.AddControllers();
builder.Services.AddTransient<IFoodService, FoodService>();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
