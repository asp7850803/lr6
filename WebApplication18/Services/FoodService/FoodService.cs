﻿using System.Net;
using System.Text;
using System.Text.Json;
using System.Xml.Linq;
using WebApplication18.Models.API;
using WebApplication18.Models.Dto;

namespace WebApplication18.Services.FoodService
{
    public class FoodService : IFoodService
    {
        private readonly HttpClient _HttpClient;
        private readonly string _apiKey;

        public FoodService(IConfiguration configuration)
        {
            _HttpClient = new HttpClient { BaseAddress = new Uri("https://api.spoonacular.com/") };
            _apiKey = configuration.GetValue<string>("foodApiKey");

        }

        public async Task<ResponseDto<SummaryRecipeResponse>> Get(int id)
        {
            try
            {
                var response = await _HttpClient.GetAsync($"recipes/{id}/summary?apiKey={_apiKey}");
                response.EnsureSuccessStatusCode();

                var responseData = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<SummaryRecipeResponse>(responseData);
                Console.WriteLine(data);
                return new ResponseDto<SummaryRecipeResponse>
                {
                    Data = new List<SummaryRecipeResponse> { data },
                    StatusCode = response.StatusCode,
                    Message = "Done"
                };
            }
            catch (HttpRequestException ex)
            {
                return new ResponseDto<SummaryRecipeResponse>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<SummaryRecipeResponse>()
                };
            }
        }

        public async Task<ResponseDto<ClassifyGroceryProductResponse>> Post(ClasifyGroceryProductRequestDto dto)
        {
            try
            {
                var jsonData = new
                {
                    plu_code = dto.PluCode ?? "",
                    title = dto.Title,
                    upc = dto.Upc ?? ""
                };
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                Console.WriteLine(json);
                var response = await _HttpClient.PostAsync($"food/products/classify?apiKey={_apiKey}", content);
                response.EnsureSuccessStatusCode();
                var responseData = await response.Content.ReadAsStringAsync();
                var data = JsonSerializer.Deserialize<ClassifyGroceryProductResponse>(responseData);
                return new ResponseDto<ClassifyGroceryProductResponse>
                {
                    Data = new List<ClassifyGroceryProductResponse> { data },
                    StatusCode = response.StatusCode,
                    Message = "Done"
                };
            }
            catch (HttpRequestException ex)
            {
                return new ResponseDto<ClassifyGroceryProductResponse>
                {
                    Message = ex.Message,
                    StatusCode = HttpStatusCode.InternalServerError,
                    Data = new List<ClassifyGroceryProductResponse>()
                };
            }
        }
    }
}
