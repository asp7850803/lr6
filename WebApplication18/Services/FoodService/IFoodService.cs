﻿using WebApplication18.Models.API;
using WebApplication18.Models.Dto;

namespace WebApplication18.Services.FoodService
{
    public interface IFoodService
    {
        public Task<ResponseDto<SummaryRecipeResponse>> Get(int id);
        public Task<ResponseDto<ClassifyGroceryProductResponse>> Post(ClasifyGroceryProductRequestDto dto);
    }
}
